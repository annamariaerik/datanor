package ee.datanor.vali.it.leiabugi.model;

import lombok.Data;

@Data
public class Person {

  private String firstName;
  private String lastName;
  private long socialSecurityId;

  public Person(String firstName, String lastName, long socialSecurityId) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.socialSecurityId = socialSecurityId;
  }

  public Person() {
  }
}

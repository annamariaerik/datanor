package ee.datanor.vali.it.leiabugi.service;

import ee.datanor.vali.it.leiabugi.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PersonService {

  List<Person> personsList = new ArrayList<>();

  @PostConstruct
  public void init(){
    Person firstP = new Person();
    firstP.setFirstName("Miki");
    firstP.setLastName("Hiir");
    firstP.setSocialSecurityId(31012340001L);

    Person secondP = new Person();
    secondP.setFirstName("Hagar");
    secondP.setLastName("Hirmus");
    secondP.setSocialSecurityId(34012340002L);

    Person thridP = new Person();
    thridP.setFirstName("Pipi");
    thridP.setLastName("Pikksukk");
    thridP.setSocialSecurityId(48012340003L);

    personsList.add(firstP);
    personsList.add(secondP);
    personsList.add(thridP);
    log.info("init done");
  }


  public Person getPerson(Long socialSecurityId){
    log.info("personList size {} {}", personsList.size(), socialSecurityId);

    Optional<Person> person = personsList.stream().filter((p -> p.getSocialSecurityId() == socialSecurityId)).findAny();
    if (person.isPresent()){
      return  person.get();
    }
    return new Person();
  }

  public Person changePerson(Long socialSecurityId, String firstName, String lastName){
    log.info("personList size {} {} {} {}", personsList.size(), socialSecurityId, firstName, lastName);

    Optional<Person> person = personsList.stream().filter(p -> p.getSocialSecurityId() == socialSecurityId).findAny();

    if (person.isPresent()){
      for (int i = 0; i < personsList.size(); i++) {
        if(personsList.get(i).getSocialSecurityId() == socialSecurityId) {
          personsList.set(i, new Person(firstName, lastName, socialSecurityId));
        }
      }
    } else if (!person.isPresent()){
      Person person1 = new Person();
      person1.setFirstName(firstName);
      person1.setLastName(lastName);
      person1.setSocialSecurityId(socialSecurityId);

      personsList.add(person1);
    }
    return new Person();
  }

  public void addPerson(Person person){
    // should check if this socialSecutiryId is already in the list,
    // if it's not, add person. If person is present, should ask, if you want to change the name

      personsList.add(person);
  }

  public List<Person> getPersons(){
    return personsList;
  }
}
